function Branch(begin, end) {
	this.begin = begin;
	this.end = end;
	this.show = function() {
		stroke(255);
		line(this.begin.x, this.begin.y, this.end.x, this.end.y);
	}
	this.branchA = function() {
		var dir = new p5.Vector.sub(this.end, this.begin);
		dir.rotate(PI / 7);
		dir.mult(0.67);
		newEnd = new p5.Vector.add(this.end, dir);

		var right = new Branch(this.end, newEnd);

		return right;	
	}
	this.branchB = function() {
		var dir = new p5.Vector.sub(this.end, this.begin);
		dir.rotate(- PI / 7);
		dir.mult(0.67);
		newEnd = new p5.Vector.add(this.end, dir);

		var left = new Branch(this.end, newEnd);

		return left;	
	}
}