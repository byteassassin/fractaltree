var angle = PI / 3;
var slider;


function setup() {
    createCanvas(600, 600);
    slider =  createSlider(0, TWO_PI, PI / 3, 0.01);
} 

function draw() {
	background(45);
	angle = slider.value();
	var len = 100;
	var swt = 10;
	translate(300,height);
	branch(120,10);
}

function branch (len,swt) {

	stroke(255);
	strokeWeight(swt);
	line(0, 0, 0, -len);
	translate(0, -len); 
	rotate(angle);
	if (len > 2) {
		push();
		rotate(angle);
		branch(len*0.75,swt*0.67);
		pop();

		push();
		rotate(-angle*3);
		branch(len*0.75,swt*0.67);
		pop();
	}
	//line(0 , 0, 0, -len*0.67);
}