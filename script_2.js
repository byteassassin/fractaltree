var root1;
var tree = [];

function setup () {
	createCanvas(1280,720);
	var a = createVector(width/2, height);
	var b = createVector(width/2, height - 100);
	root1 = new Branch(a,b);
	tree[0] = root1;
}

function mousePressed() {
	for ( var i = tree.length -1 ; i >= 0; i--) {
	tree.push(tree[i].branchA());
	tree.push(tree[i].branchB());
	}
}

function draw() {
	background(67);
	for (var i = 0; i < tree.length; i++) {
		tree[i].show();
	}
	root1.show();
}